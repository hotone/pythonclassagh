import csv

# wczytaj zawartosc pliku 'zadanie2.csv'
with open("resources/zadanie2.csv", "r") as file:
    csv_reader = csv.reader(file)
    list_of_rows = []
    counter = 0
    header = []

    for row in csv_reader:
        if counter == 0:
            header = row
        else:
            list_of_rows.append(row)
        counter += 1

    # usun z pliku linie puste oraz zamień ciagi liter, aby zaczynaly sie od wielkich liter
    for entry in list_of_rows:
        entry[1] = entry[1].capitalize()

    # posortuj linie w pliku wedlug id
    sorted_list_of_rows = sorted(list_of_rows, key=lambda x: int(x[0]))
    # napraw numeracje uzywajac nowego licznika

    # Nadanie całkiem nowej numeracji w kolejności
    # for index in range(0, counter - 1):
    #     sorted_list_of_rows[index][0] = index + 1

    # Naprawa numeracji zgodnie z zadaniem tj.
    # "w przypadku znalezienia duplikatu należy nadać numer o jeden większy od poprzedniego"
    already_existing_ids_list = []
    for row in sorted_list_of_rows:
        if int(row[0]) not in already_existing_ids_list:
            already_existing_ids_list.append(int(row[0]))
        else:
            incrementation_candidate = int(row[0]) + 1
            if incrementation_candidate < max(already_existing_ids_list):
                incrementation_candidate = max(already_existing_ids_list) + 1
            while incrementation_candidate in already_existing_ids_list:
                incrementation_candidate += 1
            already_existing_ids_list.append(incrementation_candidate)
            row[0] = str(incrementation_candidate)

    # zapis zmienione dane do nowego pliku
    with open("zadanie2-res.csv", "w") as output_file:
        csv_writer = csv.writer(output_file, delimiter=",")

        csv_writer.writerow(header)
        for row in sorted_list_of_rows:
            csv_writer.writerow(row)
