# zaimportuj wymagane moduly
import math
import time
import random
import matplotlib.pyplot as plt
import numpy as np

random.seed(time.time())

iterations_amount_ranges = (100, 1000, 10000, 100000, 1000000)

values_interval_of_square_sides = (0, 10)
circle_origin = (5, 5)
circle_radius = 5

square_side_lenght = values_interval_of_square_sides[1] - values_interval_of_square_sides[0]


def is_point_inside_circle(x, y):
    return math.pow((x - circle_origin[0]), 2) + math.pow((y - circle_origin[1]), 2) <= math.pow(circle_radius, 2)


for iterations_amount in iterations_amount_ranges:
    points_inside_circle = []
    points_outside_circle = []

    circle = plt.Circle(circle_origin, circle_radius, color='r', fill=False)

    figure, axis = plt.subplots()

    plt.xlim(values_interval_of_square_sides[0], values_interval_of_square_sides[1])
    plt.ylim(values_interval_of_square_sides[0], values_interval_of_square_sides[1])

    axis.set_aspect(1)
    axis.add_artist(circle)

    for i in range(0, iterations_amount):
        # wylosuj przykladowe liczby rzeczywiste z zakresu wspolrzednych kwadratu
        random_x_coordinate = random.uniform(values_interval_of_square_sides[0], values_interval_of_square_sides[1])
        random_y_coordinate = random.uniform(values_interval_of_square_sides[0], values_interval_of_square_sides[1])

        # dla kazdej pary wspolrzednych sprawdz czy spelniaja rownanie okregu
        if is_point_inside_circle(random_x_coordinate, random_y_coordinate):
            points_inside_circle.append([random_x_coordinate, random_y_coordinate])
        else:
            points_outside_circle.append([random_x_coordinate, random_y_coordinate])

    inside_points_data = np.array(points_inside_circle)
    outside_points_data = np.array(points_outside_circle)

    axis.scatter(inside_points_data.transpose()[0], inside_points_data.transpose()[1])
    axis.scatter(outside_points_data.transpose()[0], outside_points_data.transpose()[1])

    plt.title(f"{iterations_amount} iterations")
    plt.show()
    time.sleep(2)

    # oblicz pole kola korzystajac z proporcji wzgledem pola kwadratu
    area = (len(points_inside_circle) / (len(points_inside_circle) + len(points_outside_circle))) * math.pow(square_side_lenght, 2)
    print(f"Przybliżone pole koła o środku {circle_origin} oraz promieniu {circle_radius} przy {iterations_amount} iteracjach wynosi: {area}")




    
