import time

with open("resources/SJP/slowa.txt", "r") as dictionary_file:
    words = []
    for line in dictionary_file.readlines():
        word = line.split('\n')[0]
        words.append(word)

    received_input = input("Proszę wpisać dowolny tekst: ")
    one_word_input = False

    # Python 3.7
    # start_time = time.clock()
    # Python 3.8
    start_time = time.perf_counter()

    if len(received_input.split(" ")) == 0 or len(received_input.split(",")):
        one_word_input = True
    is_word_from_dictionary = False

    if one_word_input:
        if received_input.lower() in words:
            is_word_from_dictionary = True

    # Python 3.7
    # elapsed_time = time.clock()
    # Python 3.8
    elapsed_time = time.perf_counter()
    elapsed_time = elapsed_time - start_time

    if is_word_from_dictionary:
        print(f"Podane wyrażenie: {received_input} jest słowem wg słownika języka polskiego")
        print(f"Czas przetwarzania: {(elapsed_time * 1000.0):.2f} ms")
    else:
        print(f"Podane wyrażenie: {received_input} nie jest słowem wg słownika języka polskiego")
        print(f"Czas przetwarzania: {(elapsed_time * 1000.0):.2f} ms")
