import glob
import os
import shutil

PATH_FOR_NEW_DIRS = "created_directory"

file_list = glob.glob('resources/zadanie1/**')

try:
    os.makedirs(PATH_FOR_NEW_DIRS)
except OSError:
    print(f"Directory {PATH_FOR_NEW_DIRS} already exists!")


for file_path in file_list:
    file_name = file_path.split("/")[-1]
    dir_name = file_name[0].upper()
    destination_path = PATH_FOR_NEW_DIRS+f"/{dir_name}"
    try:
        os.makedirs(destination_path)
    except OSError:
        print(f"Directory {destination_path} already exists!")
    # instead of moving better to copy to not spoil input
    # resources in case of testing this script multiple times
    # shutil.move(file_path, destination_path)
    shutil.copy(file_path, destination_path)
