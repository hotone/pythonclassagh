import glob
import os
import shutil

PATH_FOR_NEW_DIRS = "resources/created_directory"

file_list = glob.glob('resources/zadanie1/**')

# makedirs with exist_ok=True checks if dir exists
# automatically so there is no need to catch any exceptions
os.makedirs(PATH_FOR_NEW_DIRS, exist_ok=True)

for file_path in file_list:
    file_name = file_path.split("/")[-1]
    dir_name = file_name[0].upper()
    destination_path = PATH_FOR_NEW_DIRS+f"/{dir_name}"
    os.makedirs(destination_path, exist_ok=True)
    # instead of moving better to copy to not spoil input
    # resources in case of testing this script multiple times
    # shutil.move(file_path, destination_path)
    shutil.copy(file_path, destination_path)
