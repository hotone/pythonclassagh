# zaimportuj wlasny modul
import figures

# stworz obiekt 'circle' oraz oblicz jego pole i obwod
circle = figures.Circle(radius=5)
print(f"Pole koła o promieniu 5: {circle.pole():.2f}")
print(f"Obwód koła o promieniu 5: {circle.obwod():.2f}")

# stworz obiekt 'triangle' oraz oblicz jego pole i obwod
triangle = figures.Triangle(base=5)
print(f"Pole trójkąta równobocznego o podstawie 5: {triangle.pole():.2f}")
print(f"Obwód trójkąta równobocznego o podstawie 5: {triangle.obwod():.2f}")

# stworz obiekt 'square' oraz oblicz jego pole i obwod
square = figures.Square(side=5)
print(f"Pole kwadratu o boku 5: {square.pole():.2f}")
print(f"Obwód kwadratu o boku 5: {square.obwod():.2f}")


