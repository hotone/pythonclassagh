# zaimportuj modul math
import math


# napisz klase 'circle'
class Circle:

    # napisz konstruktor klasy
    def __init__(self, radius):
        self.radius = radius

    # napisz metode liczaca pole
    def pole(self):
        return math.pi * math.pow(self.radius, 2)

    # napisz metode liczaca obwod
    def obwod(self):
        return 2 * math.pi * self.radius


# napisz klase 'triangle'
class Triangle:

    # napisz konstruktor klasy
    def __init__(self, base):
        self.base = base

    # napisz metode liczaca pole
    def pole(self):
        return (math.pow(self.base, 2) * math.sqrt(3)) / 4

    # napisz metode liczaca obwod
    def obwod(self):
        return 3 * self.base


# napisz klase 'square'
class Square:

    # napisz konstruktor klasy
    def __init__(self, side):
        self.side = side

    # napisz metode liczaca pole
    def pole(self):
        return math.pow(self.side, 2)

    # napisz metode liczaca obwod
    def obwod(self):
        return self.side * 4
